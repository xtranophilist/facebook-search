$(document).ready(function(){
    vm = new FBSearchVM('Bhairahawa');
    ko.applyBindings(vm);
});

function FBSearchVM(query){
    var self = this;
    self.access_token = '117687431628967|j-Dxqi34F0hNhqCrcJF0HLf8e4k';
    self.query = ko.observable(query);
    self.limit = 20;
    self.url = 'https://graph.facebook.com/search/?limit='+self.limit+'&q='+self.query()+'&access_token='+self.access_token;
    self.page = ko.observable(1);
    self.previous_url = ko.observable();
    self.next_url = ko.observable();

    self.posts = ko.observableArray();

    self.fetch = function(fetch_url){
        var url = fetch_url || self.url;
        $.getJSON(url, function(response){
            self.previous_url(response['paging']['previous']);
            self.next_url(response['paging']['next']);
            self.posts(ko.utils.arrayMap(response['data'], function (item) {
                return new FBPostVM(item);
            }));
        });
    }

    self.fetch();

}

function FBPostVM(data){

    var self = this;

    self.message_tags = [];

    for (var k in data){
        self[k] = data[k];
    }

    self.from['description'] = 'View profile on Facebook';

    if (self.from.category){
        self.from['description'] = 'View this ' + self.from.category + ' on Facebook';
        if (self.from.category_list){
            var categories = [];
            for (var k in self.from.category_list){
                var category = self.from.category_list[k];
                categories.push(category.name);
            }
            self.from['description'] = 'View this ' + categories.join('/') + ' on Facebook';
        }
    }

    self.play_video = function(post, e){
        el = e.currentTarget;
        $(el).find('img').remove();
        $(el).html('<iframe width=480 height=320 src="'+self.source+'"></iframe>');
        return false;
        
    }

}