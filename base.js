String.prototype.trunc =
function(n,useWordBoundary){
	var toLong = this.length>n,
	s_ = toLong ? this.substr(0,n-1) : this;
	s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
	return  toLong ? s_ + ' ... ' : s_;
};

ko.bindingHandlers.truncate = {
	init: function (element, valueAccessor, allBindingsAccessor) {
	},
	update: function (element, valueAccessor, allBindingsAccessor) {
		var full_text = valueAccessor();

		if (full_text.length <256){
			$(element).text(full_text);
			return;
		}
		var short_text = full_text.trunc(250,true)

		$(element).text(short_text);
		var appended_link = jQuery('<a/>', {
			class: 'appended-link',
			href: 'javascript:void(0)',
			title: 'Expand the text',
			text: 'See More'
		});
		appended_link.appendTo($(element)).on('click', function(){
			$(element).text(full_text);
		});
	}
}

ko.bindingHandlers.pluralize = {
	init: function (element, valueAccessor, allBindingsAccessor) {
	},
	update: function (element, valueAccessor, allBindingsAccessor) {
		if(valueAccessor() !=1)
			$(element).text($(element).text() + 's');
	}	
}

ko.bindingHandlers.parse_tags = {
	init: function (element, valueAccessor, allBindingsAccessor) {
	},
	update: function (element, valueAccessor, allBindingsAccessor) {
		if(!Object.size(valueAccessor()))
			return;
		// TODO support for multipe tags in sigle post
		var text = $(element).text();
		var tags = valueAccessor();
		for (var k in tags){
			var tag = tags[k];
			var offset = tag[0].offset;
			var pre_text = text.substr(0,offset);
			$(element).html(pre_text);
			var built_tag = jQuery('<a/>', {
				class: 'built-tag',
				href: 'http://www.facebook.com/profile.php?id='+tag[0].id,
				title: 'View '+ tag[0].name + '\'s profile in Facebook',
				text: tag[0].name,
				target: '_blank'
			});
			built_tag.appendTo($(element));
			var post_text = text.substr(offset+tag[0].length);
			$(element).append(post_text);
		}
	}		
}

Object.size = function (obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};

function get_relative_time(time_value) {
    var time_lt1min = 'less than 1 min ago';
    var time_1min = '1 min ago';
    var time_mins = '%1 mins ago';
    var time_1hour = '1 hour ago';
    var time_hours = '%1 hours ago';
    var time_1day = '1 day ago';
    var time_days = '%1 days ago';
    var parsed_date = Date.parse(time_value);
    var relative_to = (arguments.length > 1) ? arguments[1] : new Date();
    var delta = parseInt((relative_to.getTime() - parsed_date) / 1000);
    delta = delta + (relative_to.getTimezoneOffset() * 60);
    if (delta < 60) {
        return time_lt1min;
    } else if (delta < 120) {
        return time_1min;
    } else if (delta < (60 * 60)) {
        return time_mins.replace('%1', (parseInt(delta / 60)).toString());
    } else if (delta < (120 * 60)) {
        return time_1hour;
    } else if (delta < (24 * 60 * 60)) {
        return time_hours.replace('%1', (parseInt(delta / 3600)).toString());
    } else if (delta < (48 * 60 * 60)) {
        return time_1day;
    } else {
        return time_days.replace('%1', (parseInt(delta / 86400)).toString());
    }
}

function fb_format_time(time_value) {
    var time = new Date(time_value);
    time = time.toUTCString();
    var values = time.split(" ");
    return values[1] + " " + values[2] + ", " + values[4] + " " + values[3];
}